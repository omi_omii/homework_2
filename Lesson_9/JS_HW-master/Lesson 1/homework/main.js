/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;
    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

  // Вариант 1

    var main = document.getElementsByTagName('body')[0];
    var r = 0;
    var g = 0;
    var b = 0; 
    r = getRandomIntInclusive();
    g = getRandomIntInclusive();
    b = getRandomIntInclusive(); 

  function getRandomIntInclusive(min, max){
    min = Math.ceil(0);
    max = Math.floor(255);
    return Math.floor(Math.random()*(max - min + 1)) + min;
    
  }
    var ground = 'rgb('+r+','+g+','+b+')';
    console.log(ground);
    main.style.background = ground;
  
// Вариант 2
  var btn = document.getElementById('btn');
  var text = document.getElementById('text');

var randcolor = function(){
  color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
  console.log(color.length);
  if(color.length < 7){
    randcolor()
  }
}
btn.addEventListener('click', function(){
  randcolor();

  document.body.style.background = color;
  text.innerText = color;
})