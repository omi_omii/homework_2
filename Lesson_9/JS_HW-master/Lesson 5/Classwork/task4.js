  /*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.9

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

*/

  document.getElementById('input').oninput = function () {
    const stage = 3;
    let val = this.value;
    let out = '';
    for(let i=0; i<val.length; i++){
      let coded = val.charCodeAt(i);
      coded = coded + stage;
      out += String.fromCharCode(coded);
    }
    document.getElementById('out').innerHTML = out;
  }

  document.getElementById('decryptInp').oninput = function(){
    const stage = -3;
    let val = this.value;
    let out = '';
    for(let i=0; i<val.length; i++){
      let coded = val.charCodeAt(i);
      coded = coded + stage;
      out += String.fromCharCode(coded);
    }
    document.getElementById('decryptOut').innerHTML = out;
  }
