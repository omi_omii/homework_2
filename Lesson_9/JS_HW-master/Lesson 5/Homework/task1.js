/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/
let CommentsArr = [];
let ProtoComment = {
    avatarUrl: 'https://upload.wikimedia.org/wikipedia/commons/1/1e/Default-avatar.jpg',
    addLikes: function(e) {
        console.log(this);
        this.likes = e;
    }
}

let target = document.getElementById('CommentsFeed');
let ul = document.createElement('ul');
target.appendChild(ul);

function Comment(name, text, avatarUrl) {
    this.name = name;
    this.text = text;
    if (avatarUrl) this.avatarUrl = avatarUrl;
    // this.avatarUrl = avatarUrl === undefined ? ProtoComment.avatarUrl : avatarUrl;
    this.likes = 0;
    this.render = function() {
        console.log(1);
        let htmlStr = "";
        for (let key in this) {
            if (typeof this[key] != 'function') htmlStr += this[key] + "<br>";
        }
        ul.innerHTML += `<li>${htmlStr}</li>`;
    }

    CommentsArr.push(this);
}

Comment.prototype = ProtoComment;

let myComment1 = new Comment("Vitya", "Vasiliy is rocker!", "https://cs.pikabu.ru/post_img/2013/11/28/11/1385661672_1578680802.jpg"),
    myComment2 = new Comment("Maxim", "Eeeeeee rozk!", "http://argumentua.com/sites/default/files/imagecache/Full_tekst_600x/article/yakubovskiy1.jpg"),
    myComment3 = new Comment("Grisha", "Are you stupid?"),
    myComment4 = new Comment("Valeriy Yhimovich", "Me too!");

CommentsArr[1].addLikes(3);
console.log(CommentsArr);
CommentsArr.map(e => e.render());



  
 
