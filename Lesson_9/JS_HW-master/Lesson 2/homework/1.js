
  

    // Задание 1.

    // Написать скрипт который будет будет переключать вкладки по нажатию
    // на кнопки в хедере.

    // Главное условие - изменять файл HTML нельзя.

    // Алгоритм:
    //   1. Выбрать каждую кнопку в шапке
    //      + бонус выбрать одним селектором

    //   2. Повесить кнопку онклик
    //       button1.onclick = function(event) {

    //       }
    //       + бонус: один обработчик на все три кнопки

    //   3. Написать функцию которая выбирает соответствующую вкладку
    //      и добавляет к ней класс active

    //   4. Написать функцию hideAllTabs которая прячет все вкладки.
    //      Удаляя класс active со всех вкладок

    // Методы для работы:

    //   getElementById
    //   querySelector
    //   classList
    //   classList.add
    //   forEach
    //   onclick

const head = document.getElementById('buttonContainer')     
const buttons = document.querySelector(".buttonsHeader").querySelectorAll(".showButton");
const container = document.querySelector('.tabContainer');


  
let activeTabIndex;  
buttons.forEach( function(button) {
    button.onclick = function() {        
        if (activeTabIndex) {
            const hideAllTabs = container.querySelector('[data-tab="' + activeTabIndex + '"]');
            hideAllTabs.classList.remove("active");
        }
        const i = button.dataset.tab; 
        const ShowTab = container.querySelector('[data-tab="' + i + '"]');
        const Tabs = container.querySelector('[data-tab]')       
        ShowTab.classList.toggle("active");
        // ShowTab.classList.add("active");
        activeTabIndex = i;
    }
})

    hideAllTabs = function(e){
        for(let i =1; i<=3; i++){
            const tabs = container.querySelector('[data-tab="'+ i + '"]');
            tabs.classList.remove('active');
        }
    }

            head.onclick = function(e){
                if (e.target==head){
                    hideAllTabs();
                }
            }


