/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

*/


let div = document.createElement('div');
let x = `
  <h3>Encrypt</h3>
    <textarea  id="encrypt"> </textarea>
    <hr>
    <p id="encryptOut"></p>
    <h3>Decrypt</h3>
    <textarea  id="decrypt"></textarea>
    <hr>
    <p id="decryptOut"></p>
`;
div.innerHTML = x;
document.body.appendChild(div)



document.getElementById('encrypt').oninput = function () {
    let stage = 3;
    let val = this.value;
    let out = '';
    for(let i=0; i<val.length; i++){
      let coded = val.charCodeAt(i);
      coded = coded + stage;
      out += String.fromCharCode(coded);
    }
    document.getElementById('encryptOut').innerHTML = out;
  }

  document.getElementById('decrypt').oninput = function(){
    let stage = -3;
    let val = this.value;
    let out = '';
    for(let i=0; i<val.length; i++){
      let coded = val.charCodeAt(i);
      coded = coded + stage;
      out += String.fromCharCode(coded);
    }
    document.getElementById('decryptOut').innerHTML = out;
  }
