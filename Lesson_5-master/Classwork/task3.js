/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

function Dog(name, breed){
    this.name = name;
    this.breed =  breed;
    this.status = 'stay';
    this.run = function(){
        this.status = 'runing'
        console.log('dog', this.status)
    }
    this.eat = function(){
        this.status = 'eating'
        console.log('dog', this.status)
    }
    this.get = function(){
        for(key in this){
            console.log(key, this[key])
        }
    }
}
var Doooog = new Dog('Tolyan', 'poodle');
Doooog.run();
Doooog.eat();
Doooog.get();
console.log(Doooog)