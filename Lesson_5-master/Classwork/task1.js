/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

let train = {
	name: 'Бурьонка',
	speed: '100 km/h',
	quantity: 10,
	move: function(){
		console.log('Поезд ' + this.name + ' везет ' + this.quantity + ' со скоростью ' + this.speed);
	},
	stop: function(){
		let speed = '0 km/h';
		console.log('Поезд ' + this.name + ' остановился.' + ' Скорость ' + speed);
	},
	pickUp: function(x){
		// let x = +prompt('quantity');
		this.quantity += Number(x);
		console.log('Количество пассажиров увеличилось на ' + x + '.' + 'Общее количество пассажиров ' + this.quantity)
	}
}
train.move()
train.stop()
train.pickUp('123')