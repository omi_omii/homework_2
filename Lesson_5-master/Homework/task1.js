/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/



let CommentsArr = [];

let myComment1 = new Comment('MOMO', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora totam nemo dolorem provident cumque, impedit error consequuntur deserunt quo, inventore. Quam quibusdam odio amet natus, optio soluta sunt? Quidem, impedit.', 'http://www.topnews.ru/upload/news/2018/08/cc0b7ac4/cc0b7ac4_1.jpg'),  
    myComment2 = new Comment('Semyon Slepakov', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam eos earum pariatur aperiam, amet facere inventore nobis ratione quis ipsum impedit quaerat, odit dicta harum nulla natus! Consequuntur, perspiciatis, facilis!', 'https://snob.ru/i/indoc/5b/rubric_issue_event_992244.jpg'),
    myComment3 = new Comment('Sergey Shnurov', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam eveniet nemo incidunt placeat suscipit magni tenetur, fugit quos ab nihil similique, illo quidem autem minima? Ipsum aperiam sapiente dolorum perferendis.', 'https://gordonua.com/img/article/1499/27_tn.jpg'),
    myComment4 = new Comment('Valentin Strikalo', "Mom I'm gay!", 'https://img.discogs.com/wgJAmzGb_MgMrYRz5q4jJX4hX2c=/fit-in/300x300/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/A-2277222-1307442005.jpeg.jpg');


let target = document.getElementById('CommentsFeed');
let ul = document.createElement('ul');
target.appendChild(ul);

function Humans(name, text, avatarUrl) {
    this.name = name;
    this.text = text;
    if (avatarUrl) this.avatarUrl = avatarUrl;
    this.likes = 0;
    this.render = function() {
        let htmlStr = "";
        for (let key in this) {
            if (typeof this[key] != 'function') {
              htmlStr += this[key] + "<br>";
            }
        }
        ul.innerHTML += `<li>${htmlStr}</li>`;
    }

    CommentsArr.push(this);
}

let ProtoComment = {
    avatarUrl: 'https://upload.wikimedia.org/wikipedia/commons/1/1e/Default-avatar.jpg',
    addLikes: function(e) {
        console.log(this);
        this.likes = e;
    }
}

Humans.prototype = ProtoComment;

CommentsArr.map(e => e.render());

