
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */


//--------------------------------------------------------------


let all        = document.querySelectorAll('.tab');
let btns         = document.querySelectorAll('.showButton');
let tabContainer   = document.querySelector('.tabContainer');
const buttonContainer  = document.getElementById('buttonContainer');


let activeTab;
btns.forEach(function(btn) {
  btn.onclick =  function(evt) {
    if(activeTab){
      let removeTab = document.querySelector(`[data-tab="${activeTab}"]`);
          removeTab.classList.remove('active')
    }
    let index = btn.dataset.tab;
    let active = tabContainer.querySelector(`[data-tab="${index}"]`);
    console.log(active)
    active.classList.toggle('active');
    activeTab = index
  }
})

function hideAllTabs(){
    all.forEach(function(tab) {
      tab.classList.remove('active');
    })
  }

    buttonContainer.onclick = function(close){
        if(close.target == buttonContainer){
          hideAllTabs()
        }
      }
